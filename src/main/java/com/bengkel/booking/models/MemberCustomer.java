package com.bengkel.booking.models;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MemberCustomer extends Customer {
    private double saldoCoin;
    public static final double DISCOUNT_RATE = 0.1; // Define the DISCOUNT_RATE as a public static final constant

    public MemberCustomer(String customerId, String name, String address, String password, List<Vehicle> vehicles,
                          double saldoCoin) {
        super(customerId, name, address, password, vehicles);
        this.saldoCoin = saldoCoin;
        setMaxNumberOfService(2);
    }


	public boolean payWithCoin(double amount) {
		if (amount <= saldoCoin) {
			saldoCoin -= amount;
			return true;
		}
		return false;
	}


	public double topUpCoin(double amount) {
		saldoCoin += amount;
		return amount;
	}

}
