package com.bengkel.booking.models;

import java.util.List;
import java.util.ArrayList;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Customer {
	private String customerId;
	private String name;
	private String address;
	private String password;
	private List<Vehicle> vehicles;
	private List<ItemService> services; // Add services field
	private int maxNumberOfService;

	public Customer(String customerId, String name, String address, String password, List<Vehicle> vehicles) {
		super();
		this.customerId = customerId;
		this.name = name;
		this.address = address;
		this.password = password;
		this.vehicles = vehicles;
		this.services = new ArrayList<>(); // Initialize services list
		this.maxNumberOfService = 1;
	}

	public Vehicle getVehicleById(String vehicleId) {
			for (Vehicle vehicle : vehicles) {
					if (vehicle.getVehiclesId().equals(vehicleId)) {
							return vehicle;
					}
			}
			return null; // Return null if vehicle not found
	}

	public boolean hasVehicle(String vehicleId) {
		for (Vehicle vehicle : vehicles) {
				if (vehicle.getVehiclesId().equals(vehicleId)) {
						return true;
				}
		}
		return false;
	}
	public int getNumServices() {
		return services.size();
	}

	public List<Vehicle> getVehicles() {
			return vehicles;
	}

	public void addService(ItemService service) {
			services.add(service);
	}
}
