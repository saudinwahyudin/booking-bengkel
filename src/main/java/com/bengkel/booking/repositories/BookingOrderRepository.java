package com.bengkel.booking.repositories;

import com.bengkel.booking.models.BookingOrder;

import java.util.ArrayList;
import java.util.List;

public class BookingOrderRepository {
    private static List<BookingOrder> bookingOrders = new ArrayList<>();

    public static void addBookingOrder(BookingOrder order) {
        bookingOrders.add(order);
    }

    public static List<BookingOrder> getBookingOrdersByCustomerId(String customerId) {
        List<BookingOrder> customerBookings = new ArrayList<>();
        for (BookingOrder order : bookingOrders) {
            if (order.getCustomer().getCustomerId().equals(customerId)) {
                customerBookings.add(order);
            }
        }
        return customerBookings;
    }
}
