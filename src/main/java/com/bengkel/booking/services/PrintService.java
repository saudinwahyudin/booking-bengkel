package com.bengkel.booking.services;

import java.util.List;

import com.bengkel.booking.models.BookingOrder;
import com.bengkel.booking.models.Car;
import com.bengkel.booking.models.ItemService;
import com.bengkel.booking.models.Vehicle;
import java.text.SimpleDateFormat;

public class PrintService { 	
	
	public static void printMenu(String[] listMenu, String title) {
		String line = "+---------------------------------+";
		int number = 1;
		String formatTable = " %-2s. %-25s %n";
		
		System.out.printf("%-25s %n", title);
		System.out.println(line);
		
		for (String data : listMenu) {
			if (number < listMenu.length) {
				System.out.printf(formatTable, number, data);
			}else {
				System.out.printf(formatTable, 0, data);
			}
			number++;
		}
		System.out.println(line);
		System.out.println();
	}
	
	public static void printVechicle(List<Vehicle> listVehicle) {
		String formatTable = "| %-2s | %-15s | %-10s | %-15s | %-15s | %-5s | %-15s |%n";
		String line = "+----+-----------------+------------+-----------------+-----------------+-------+-----------------+%n";
		System.out.format(line);
	    System.out.format(formatTable, "No", "Vechicle Id", "Warna", "Brand", "Transmisi", "Tahun", "Tipe Kendaraan");
	    System.out.format(line);
	    int number = 1;
	    String vehicleType = "";
	    for (Vehicle vehicle : listVehicle) {
	    	if (vehicle instanceof Car) {
				vehicleType = "Mobil";
			}else {
				vehicleType = "Motor";
			}
	    	System.out.format(formatTable, number, vehicle.getVehiclesId(), vehicle.getColor(), vehicle.getBrand(), vehicle.getTransmisionType(), vehicle.getYearRelease(), vehicleType);
	    	number++;
	    }
	    System.out.printf(line);
	}
	

	public static void printServices(List<ItemService> listServices) {
        String formatTable = "| %-2s | %-15s | %-15s | %-15s | %-15s |%n";
        String line = "+----+-----------------+-----------------+-----------------+-----------------+%n";
        System.out.format(line);
        System.out.format(formatTable, "No", "Service Id", "Nama Service", "Tipe Kendaraan", "Harga");
        System.out.format(line);
        int number = 1;
        for (ItemService service : listServices) {
            // Sesuaikan dengan atribut-atribut yang ada pada ItemService
            System.out.format(formatTable, number, service.getServiceId(), service.getServiceName(), service.getVehicleType(), service.getPrice());
            number++;
        }
        System.out.printf(line);
    }

		public static void printBookingOrders(List<BookingOrder> bookingOrders) {
			String formatTable = "| %-2s | %-15s | %-15s | %-15s | %-15s | %-15s | %-15s |%n";
			String line = "+----+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+%n";
			System.out.println("Booking Order Menu");
			System.out.format(line);
			System.out.format(formatTable, "No", "Booking Id", "Nama Customer", "Payment Method", "Total Service", "Total Payment", "List Service");
			System.out.format(line);
			int number = 1;
			for (BookingOrder order : bookingOrders) {
					StringBuilder servicesList = new StringBuilder();
					for (ItemService service : order.getServices()) {
							servicesList.append(service.getServiceName()).append(", ");
					}
					// Remove the last comma and space
					if (servicesList.length() > 0) {
							servicesList.setLength(servicesList.length() - 2);
					}
					System.out.format(formatTable, number, order.getBookingId(), order.getCustomer().getName(), order.getPaymentMethod(), order.getTotalServicePrice(), order.getTotalPayment(), servicesList.toString());
					number++;
			}
			System.out.format(line);
			System.out.println("0\tKembali Ke Main Menu");
	}
    // ...
	//Silahkan Tambahkan function print sesuai dengan kebutuhan.
	
}
