package com.bengkel.booking.services;

import java.util.List;
import java.util.Scanner;
import java.util.ArrayList;

import com.bengkel.booking.models.BookingOrder;
import com.bengkel.booking.models.Customer;
import com.bengkel.booking.models.ItemService;
import com.bengkel.booking.models.MemberCustomer;
import com.bengkel.booking.repositories.BookingOrderRepository;
import com.bengkel.booking.repositories.CustomerRepository;
import com.bengkel.booking.repositories.ItemServiceRepository;

public class MenuService {
    private static Scanner input = new Scanner(System.in);
    private static Customer loggedInCustomer;

    public static void run() {
        String[] startMenuArr = {"Login", "Exit"};
        boolean backToStartMenu = false;

        do {
            PrintService.printMenu(startMenuArr,"Aplikasi Booking Bengkel");
            int optionStartMenu = Integer.valueOf(input.nextLine());
            switch (optionStartMenu) {
                case 0:
                    backToStartMenu = true;
                    break;
                case 1:
                    login();
                    break;
                default:
                    break;
            }
        } while (!backToStartMenu);
    }

    public static void login() {
        System.out.println("+------------Login----------------+");
        System.out.print("Masukan Customer Id: ");
        String customerId = input.nextLine();
        System.out.print("Masukan Password: ");
        String password = input.nextLine();

        boolean loginStatus = BengkelService.login(customerId, password);
        if (loginStatus) {
            loggedInCustomer = CustomerRepository.getCustomerById(customerId);
            mainMenu();
        } else {
            System.out.println("Login gagal. Periksa kembali Customer Id dan Password Anda.");
        }
    }

    public static void mainMenu() {
        String[] listMenu = {"Informasi Customer", "Booking Bengkel", "Top Up Bengkel Coin", "Informasi Booking", "Logout"};
        int menuChoice;
        boolean isLooping = true;

        do {
            PrintService.printMenu(listMenu, "Selamat Datang Di Aplikasi Booking Bengkel Menu");
            menuChoice = Integer.valueOf(input.nextLine());

            switch (menuChoice) {
                case 1:
                    if (loggedInCustomer != null) {
                        BengkelService.getCustomerInfo(loggedInCustomer.getCustomerId());
                    } else {
                        System.out.println("Anda belum login. Silakan login terlebih dahulu.");
                    }
                    break;
                case 2:
                    if (loggedInCustomer != null) {
                        performBooking();
                    } else {
                        System.out.println("Anda belum login. Silakan login terlebih dahulu.");
                    }
                    break;
                case 3:
                    if (loggedInCustomer != null && loggedInCustomer instanceof MemberCustomer) {
                        performTopUpCoin();
                    } else {
                        System.out.println("Maaf fitur ini hanya untuk Member saja!");
                    }
                    break;
                case 4:
                    if (loggedInCustomer != null) {
                        showBookingInformation();
                    } else {
                        System.out.println("Anda belum login. Silakan login terlebih dahulu.");
                    }
                    break;
                case 0:
                    System.out.println("Logout");
                    isLooping = false;
                    break;
                default:
                    System.out.println("Pilihan tidak valid.");
                    break;
            }
        } while (isLooping);
    }

    public static void performBooking() {
        System.out.print("Masukkan Vehicle Id: ");
        String vehicleId = input.nextLine();

        if (!loggedInCustomer.hasVehicle(vehicleId)) {
            System.out.println("Kendaraan dengan Vehicle Id " + vehicleId + " tidak ditemukan.");
            return;
        }

        List<ItemService> servicesForVehicle = ItemServiceRepository.getItemServicesForVehicle(loggedInCustomer.getVehicleById(vehicleId));

        if (servicesForVehicle.isEmpty()) {
            System.out.println("Tidak ada layanan yang tersedia untuk kendaraan ini.");
            return;
        } else {
            System.out.println("List Service yang Tersedia:");
            PrintService.printServices(servicesForVehicle);
        }

        boolean addMoreServices;
        double totalHarga = 0.0;
        List<ItemService> chosenServices = new ArrayList<>();
        do {
            System.out.print("Silahkan masukkan Service Id: ");
            String serviceId = input.nextLine();

            ItemService chosenService = null;
            for (ItemService service : servicesForVehicle) {
                if (service.getServiceId().equals(serviceId)) {
                    chosenService = service;
                    break;
                }
            }

            if (chosenService == null) {
                System.out.println("Service dengan ID " + serviceId + " tidak valid.");
                break;
            }

            chosenServices.add(chosenService);
            totalHarga += chosenService.getPrice();

            System.out.print("Apakah Anda ingin menambahkan Service Lainnya? (Y/T): ");
            String addMore = input.nextLine().trim();
            addMoreServices = addMore.equalsIgnoreCase("Y");
        } while (addMoreServices);

        System.out.println("Silahkan Pilih Metode Pembayaran (Saldo Coin atau Cash)");
        System.out.print("Metode Pembayaran: ");
        String paymentMethod = input.nextLine();

        double totalPembayaran = totalHarga;
        if (paymentMethod.equalsIgnoreCase("Saldo Coin") && loggedInCustomer instanceof MemberCustomer) {
            totalPembayaran *= 0.9;
        }

        BookingOrder bookingOrder = new BookingOrder();
        bookingOrder.setBookingId("Book-" + loggedInCustomer.getCustomerId() + "-" + (BookingOrderRepository.getBookingOrdersByCustomerId(loggedInCustomer.getCustomerId()).size() + 1));
        bookingOrder.setCustomer(loggedInCustomer);
        bookingOrder.setServices(chosenServices);
        bookingOrder.setPaymentMethod(paymentMethod);
        bookingOrder.setTotalServicePrice(totalHarga);
        bookingOrder.setTotalPayment(totalPembayaran);

        BookingOrderRepository.addBookingOrder(bookingOrder);

        System.out.println("\nBooking Berhasil!!!");
        System.out.println("Total Harga Service: " + totalHarga);
        System.out.println("Total Pembayaran: " + totalPembayaran);
    }

    public static void performTopUpCoin() {
        System.out.println("Top Up Saldo Coin");
        System.out.print("Masukkan besaran Top Up: ");
        double topUpAmount = Double.parseDouble(input.nextLine());
        ((MemberCustomer) loggedInCustomer).topUpCoin(topUpAmount);
        System.out.println("Top Up berhasil. Saldo Coin Anda sekarang: " + ((MemberCustomer) loggedInCustomer).getSaldoCoin());
    }

    public static void showBookingInformation() {
        List<BookingOrder> bookingOrders = BookingOrderRepository.getBookingOrdersByCustomerId(loggedInCustomer.getCustomerId());
        if (bookingOrders.isEmpty()) {
            System.out.println("Tidak ada booking order yang ditemukan untuk customer ini.");
        } else {
            PrintService.printBookingOrders(bookingOrders);
        }
    }
}
