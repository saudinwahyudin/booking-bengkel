package com.bengkel.booking.services;

import java.util.ArrayList;
import java.util.List;

import com.bengkel.booking.models.BookingOrder;
import com.bengkel.booking.models.Customer;
import com.bengkel.booking.models.ItemService;
import com.bengkel.booking.models.MemberCustomer;
import com.bengkel.booking.repositories.CustomerRepository;
import com.bengkel.booking.repositories.ItemServiceRepository;
import com.bengkel.booking.repositories.BookingOrderRepository;

public class BengkelService {
	
	//Silahkan tambahkan fitur-fitur utama aplikasi disini
	
	//Login
	public static boolean login(String customerId, String password) {
        // Mendapatkan daftar semua pelanggan dari repository
        // Menggunakan metode getAllCustomer dari CustomerRepository
        List<Customer> listAllCustomers = CustomerRepository.getAllCustomer();

        for (Customer customer : listAllCustomers) {
            if (customer.getCustomerId().equals(customerId)) {
                if (customer.getPassword().equals(password)) {
                    System.out.println("Login berhasil!");
                    return true; // Jika login berhasil, kembalikan true
                } else {
                    System.out.println("Password yang Anda masukkan salah!");
                    return false; // Jika password salah, kembalikan false
                }
            }
        }
        // Jika customerId tidak ditemukan
        System.out.println("Customer Id tidak ditemukan atau salah!");
        return false;
    }
	
	//Info Customer
	public static void getCustomerInfo(String customerId) {
		List<Customer> customers = CustomerRepository.getAllCustomer();

		for (Customer customer : customers) {
				if (customer.getCustomerId().equals(customerId)) {
						System.out.println("Customer Profile");
						System.out.println("\tCustomer Id: " + customer.getCustomerId());
						System.out.println("\tNama: " + customer.getName());
						System.out.println("\tAlamat: " + customer.getAddress());

						if (customer instanceof MemberCustomer) {
								MemberCustomer member = (MemberCustomer) customer;
								System.out.println("\tCustomer Status: Member");
								System.out.println("\tSaldo Koin: " + member.getSaldoCoin());
						} else {
								System.out.println("\tCustomer Status: Non Member");
						}

						System.out.println("\tList Kendaraan:");
						PrintService.printVechicle(customer.getVehicles());
						return;
				}
		}

		System.out.println("Customer dengan ID " + customerId + " tidak ditemukan.");
  }
	
	//Booking atau Reservation
	public static void bookingService(String customerId, String vehicleId, int serviceChoice, int paymentMethod) {
		Customer customer = CustomerRepository.getCustomerById(customerId);
		if (customer != null) {
				if (customer.hasVehicle(vehicleId)) {
						List<ItemService> servicesForVehicle = ItemServiceRepository.getItemServicesForVehicle(customer.getVehicleById(vehicleId));
						if (servicesForVehicle.isEmpty()) {
								System.out.println("Tidak ada layanan yang tersedia untuk kendaraan ini.");
						} else {
								if (serviceChoice >= 1 && serviceChoice <= servicesForVehicle.size()) {
										ItemService chosenService = servicesForVehicle.get(serviceChoice - 1);
										int maxServices = customer instanceof MemberCustomer ? 2 : 1;
										if (customer.getNumServices() < maxServices) {
												if (paymentMethod == 1 && customer instanceof MemberCustomer) {
														// Member dengan metode pembayaran Saldo Coin
														double totalCost = chosenService.getPrice() * (1 - MemberCustomer.DISCOUNT_RATE);
														if (((MemberCustomer) customer).payWithCoin(totalCost)) {
																customer.addService(chosenService);
																System.out.println("Booking berhasil.");
														} else {
																System.out.println("Saldo Coin tidak mencukupi.");
														}
												} else if (paymentMethod == 2) {
														// Pembayaran dengan Cash
														customer.addService(chosenService);
														System.out.println("Booking berhasil.");
												} else {
														System.out.println("Metode pembayaran tidak valid.");
												}
										} else {
												System.out.println("Anda telah mencapai batas jumlah layanan untuk booking ini.");
										}
								} else {
										System.out.println("Pilihan layanan tidak valid.");
								}
						}
				} else {
						System.out.println("Kendaraan tidak ditemukan.");
				}
		} else {
				System.out.println("Anda belum login. Silakan login terlebih dahulu.");
		}
  }

	//Top Up Saldo Coin Untuk Member Customer
	public static void topUpCoin(String customerId, double amount) {
		Customer customer = CustomerRepository.getCustomerById(customerId);
		if (customer instanceof MemberCustomer) {
			MemberCustomer member = (MemberCustomer) customer;
			member.topUpCoin(amount);
			System.out.println("Top Up berhasil. Saldo Coin Anda sekarang: " + member.getSaldoCoin());
		} else {
			System.out.println("Maaf, hanya Member yang dapat melakukan top up saldo coin.");
		}
	}
	

	// Menampilkan Informasi Booking Order
	public static void getBookingInfo(String customerId) {
		List<BookingOrder> bookingOrders = BookingOrderRepository.getBookingOrdersByCustomerId(customerId);
		if (bookingOrders.isEmpty()) {
				System.out.println("Tidak ada booking order yang ditemukan untuk customer ini.");
		} else {
				PrintService.printBookingOrders(bookingOrders);
		}
}


//Logout
	
}
